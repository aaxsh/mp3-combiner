# mp3-combiner
A fast easy to use mp3 file combiner.

## **Usage**
In the mp3 folder you have to put the mp3 files, which you want combine. 
To run the program you have to execute >`audio.py`
In the output file will be the output.mp3

***Attention:*** The mp3-files haveto be in the right order. The programm will take by default the order of the file!
